
import React,{Component } from 'react';
import './App.css'
import {Form, FormControl, Button} from 'react-bootstrap';

class StopWatch extends Component{
	constructor(props){
		super(props);
		this.state = {
		 secondsremaining: parseInt(this.props.deadline),	
      secondsleft: 0.5,
      minutesleft: 0.5,
      counter:0.5,
      newDeaDLine : 0.5
		}
	}
	componentWillMount(){
		this.getTimeUntil();
	}

	getTimeUntil(){
	
	if(this.state.secondsremaining != 0) {
		 this.setState({secondsremaining: this.state.secondsremaining - 1});
      this.setState({secondsleft: Math.floor(this.state.secondsremaining % 60)});
      this.setState({minutesleft: Math.floor(this.state.secondsremaining / 60)});
     }
 
  	 if (this.state.secondsleft <=0 && this.state.minutesleft <=0) {
  	 	alert("Time's up!")
  	 	 this.setState({secondsleft: 0.1});
      this.setState({minutesleft: 0.1});
    }
 
 
     
	}

	componentDidMount(){
       setInterval(() => this.getTimeUntil(),1000);

	}

changeDeadLine (){
  
  this.setState({secondsremaining:this.state.newDeaDLine});
document.getElementById("someName").value= "";
 }

	

render (){
	return(
	<div>
      <p className="clock-hours">
      {this.state.minutesleft} minutes and {this.state.secondsleft} seconds remaining
      </p>  
       <Form inline>
      <FormControl id="someName" className= "form-input" placeholder="Input Time: Eg- 10" onChange={event => this.setState({newDeaDLine:event.target.value})}/>
      <Button  className = "button-click " onClick={() => this.changeDeadLine()}>Submit</Button>
      </Form>  
      </div>

	)
}



}


export default StopWatch;