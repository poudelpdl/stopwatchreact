import React, { Component } from 'react';
import Stopwatch from './StopWatch';
import logo from './logo.svg';
import './App.css';


class App extends Component {
constructor(props){
  super(props);
  this.state={
    deadline:10,
    
  }
}


  render() {
    return (
      <div className="App">
        <div className="App-title">Stop Watch</div>
        <Stopwatch deadline={this.state.deadline}/>
      
   
      </div>
    );
  }
}

export default App;
